# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class CidadeItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    nome = scrapy.Field()
    href = scrapy.Field()


class CarroItem(scrapy.Item):
    numero =         scrapy.Field() 
    categoria =      scrapy.Field() 
    marca =          scrapy.Field() 
    modelo =         scrapy.Field() 
    ano =            scrapy.Field() 
    cor =            scrapy.Field() 
    combustivel =    scrapy.Field() 
    cambio =         scrapy.Field() 
    portas =         scrapy.Field() 
    km =             scrapy.Field() 
    placa =          scrapy.Field() 
    opcionais =      scrapy.Field() 
