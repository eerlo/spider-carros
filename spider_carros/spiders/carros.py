# -*- coding: utf-8 -*-

import scrapy
from spider_carros.items import CidadeItem, CarroItem
from scrapy.utils.url import urljoin_rfc
from scrapy.utils.response import get_base_url

class CarrosSpider(scrapy.Spider):
    name = 'carrosspider'

    start_urls = ['http://www.carrosnaserra.com.br/revendas']

    def parse(self, response):
        nomes = response.selector.xpath("//div[@id='lista']/ul/li/a/text()").extract()
        urls = response.selector.xpath("//div[@id='lista']/ul/li/a/@href").extract()
        links = [urljoin_rfc(get_base_url(response), x) for x in urls]

        for i in links:
            yield scrapy.Request(i, callback=self.parse_revendas)


    def parse_revendas(self, response):
        nomes = response.selector.xpath("//div[@id='lista']/ul/li/a/text()").extract()
        urls = response.selector.xpath("//div[@id='lista']/ul/li/a/@href").extract()
        links = [urljoin_rfc(get_base_url(response), x) for x in urls]
        for i in links:
            yield scrapy.Request(i, callback=self.parse_carros)


    def parse_carros(self, response):
        carros = response.selector.xpath(
                "//section[@id='conteudo']//table//tr//a/@href").extract()
        links = [urljoin_rfc(get_base_url(response), x) for x in carros]

        for i in links:
            yield scrapy.Request(i, callback=self.parse_carro)


    def parse_carro(self, response):
        dados = response.selector.xpath("//div[@id='anuncioesq']/ul/li/text()").extract()
        numero = dados[0]
        categoria = dados[1]
        marca = dados[2]
        modelo = dados[3]
        ano = dados[4]
        cor = dados[5]
        combustivel = dados[6]
        cambio = dados[7]
        portas = dados[8]
        km = dados[8]
        placa = dados[9]

        return CarroItem(numero=numero, categoria=categoria,
                marca=marca, modelo=modelo, ano=ano, cor=cor,
                combustivel=combustivel,cambio=cambio,
                portas=portas,km=km,placa=placa)
